import { Component, OnInit } from '@angular/core';
import { ResumeService } from './resume.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {
  resumeGroup: any;
  constructor(public fb: FormBuilder, public resumeService: ResumeService) {
    this.formCreate();
  }
  formCreate() {
    this.resumeGroup = this.fb.group({
      title: ['', Validators.required],
      name: ['', { validators: this.demoValidator }],
      fatherName: [''],
      dob: ['', Validators.required],
      mobile: ['', [Validators.required, Validators.pattern('(0/91)?[7-9][0-9]{9}')]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required],
      gender: ['', Validators.required],
      objective: ['', Validators.required],
      expYear: ['', Validators.required],
      expMonth: ['', Validators.required],
      sccPer: ['', Validators.required],
      sccBoard: ['', Validators.required],
      sccYear: ['', Validators.required],
      hscPer: ['', Validators.required],
      hscBoard: ['', Validators.required],
      hscYear: ['', Validators.required],
      graduation: ['', Validators.required],
      gradPer: ['', Validators.required],
      grdClg: ['', Validators.required],
      grdYear: ['', Validators.required],
      postGraduation: [''],
      postPer: [''],
      postClg: [''],
      postYear: [''],
      skills: ['', Validators.required],
      projectDetails: ['', Validators.required],
      hobbies: ['', Validators.required],
      img: ['', Validators.required]
      },{ validators:this.myValidator })


  }
  myValidator(control:FormGroup){
    console.log("hii");
    var controlF =control.get("fatherName");
    if (!controlF.value.length) { 
       return { required: true }
    }

  }

  demoValidator(control: FormControl) {
    var nameReg: string = control.value;
    var reg = /^[a-zA-Z]*$/;
    console.log(reg);
    console.log(reg.test(nameReg));
    if (!nameReg.length) {
      console.log("gg");
      return { required: true };
    }
    else if (!reg.test(nameReg)) {
      return { pattern: true }
    }
  }
  ngOnInit() {
    this.getData();
  }
  getData() {
    this.resumeGroup.patchValue(this.resumeService.getResume());
  }
  submitForm() {
    let formData = this.resumeGroup.value;
    this.resumeService.savaResume(formData);
    // console.log(this.resumeGroup.get('name').errors);
    console.log(this.resumeGroup.controls.name.errors);
    console.log(this.resumeGroup.controls.title.errors);
    console.log(this.resumeGroup.controls.email);
  }
}
