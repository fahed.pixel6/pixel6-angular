import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ResumeService {
  postGraduations = [
    {
      id: 1,
      value: "M.E"
    }, {
      id: 2,
      value: "M.Tech"
    },{
      id:3,
      value:"MCA"
    },{
      id:4,
      value:"MSC"
    },{
      id:5,
      value:"MBA"
    },{
      id:6,
      value:"M.A"
    }
  ];
  graduations = [
    {
      id: 1,
      value: "B.E"
    }, {
      id: 2,
      value: "BCA"
    }, {
      id: 3,
      value: "BSC"
    }
  ];
  titleList = [
    {
      id: 1,
      value: "Mr."
    },
    {
      id: 2,
      value: "Mrs."
    }
  ];
  expYear=[
    {
      id:1,
      value:1
    },
    {
      id:2,
      value:2
    },
    {
      id:3,
      value:3
    },
    {
      id:4,
      value:4
    },
    {
      id:5,
      value:5
    },
    {
      id:6,
      value:6
    }
  ];
 expMonth=[
   {
     id:1,
     value:1
   },
   {
    id:2,
    value:2
  },
  {
    id:3,
    value:3
  },
  {
    id:4,
    value:4
  },
  {
    id:5,
    value:5
  },
  {
    id:6,
    value:6
  },
  {
    id:7,
    value:7
  },
  {
    id:8,
    value:8
  },
  {
    id:9,
    value:9
  },
  {
    id:10,
    value:10
  },
  {
    id:11,
    value:11
  }
 ];
 board=[
  {
    id:1,
    value:"Amravati",
  },
  {
    id:2,
    value:"CBSC"
  }
];
  Year=[
    {
      id:1,
      value:"2010"
    },
    {
      id:2,
      value:"2011"
    },
    {
      id:3,
      value:"2012"
    },
    {
      id:4,
      value:"2013"
    },
    {
      id:5,
      value:"2014"
    },
    {
      id:6,
      value:"2015"
    },
    {
      id:7,
      value:"2016"
    },
    {
      id:8,
      value:"2017"
    },
    {
      id:9,
      value:"2018"
    }
  ]

  resume = {
    title:"",
    name: "",
    fatherName: "",
    dob: "",
    mobile: "",
    email: "",
    address: "",
    gender: "", 
    objective: "",
    expYear:"",
    expMonth:"",
    sccPer:"",
    sccBoard:"",
    sccYear:"",
    hscPer:"",
    hscBoard:"",
    hscYear:"",
    graduation:"",
    gradPer:"",
    grdClg:"",
    grdYear:"",
    postGraduation:"",
    postPer:"",
    postClg:"",
    postYear:"",
    skills:"",
    projectDetails:"",
    hobbies:"",
    img:""
  }
  constructor() { }
  savaResume(reqestData:any){
      console.log(reqestData);
  }
  getResume(){
    return this.resume;
  }

}
