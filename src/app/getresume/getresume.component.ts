import { Component, OnInit } from '@angular/core';
import {ResumeService } from '../resume/resume.service'

@Component({
  selector: 'app-getresume',
  templateUrl: './getresume.component.html',
  styleUrls: ['./getresume.component.css']
})
export class GetresumeComponent implements OnInit {

  constructor(public resumeService: ResumeService) { 

  }

  ngOnInit() {
  }

}
