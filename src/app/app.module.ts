import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ResumeComponent } from './resume/resume.component';
import { ReactiveFormsModule,FormsModule} from '@angular/forms';
import { FormgroupComponent } from './formgroup/formgroup.component';
import { GetresumeComponent } from './getresume/getresume.component';

@NgModule({
  declarations: [
    AppComponent,
    ResumeComponent,
    FormgroupComponent,
    GetresumeComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
